import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    todolist: [
      {
        id: 'sdfqffdgvdffkkvd',
        email: 'gilleskidjo@gmail.com',
        project: [
          {
            nameProjet: 'Figma',
            myTodolist: [
              {
                titleOfTask: 'Lire Figma',
                dateStart: '15-12-2019',
                dateEnd: '16-12-2019',
                isClose: false,
                description: 'En gros c"est de lire Figma seulement simple'
              }
            ]
          }
        ]
      },
      {
        id: 'sdfqffdgvdffkkvd',
        email: 'gilleskidjo@gmail.com',
        project: [
          {
            nameProjet: 'Bulma',
            myTodolist: [
              {
                titleOfTask: 'Lire Bulma',
                dateStart: '11-12-2019',
                dateEnd: '15-12-2019',
                isClose: false,
                description: 'En gros c"est de lire Bulma seulement simple'
              }
            ]
          }
        ]
      },
      {
        id: 'sdfqffdgvdffkkvd',
        email: 'gilleskidjo@gmail.com',
        project: [
          {
            nameProjet: 'Vuejs',
            myTodolist: [
              {
                titleOfTask: 'Lire Vuejs',
                dateStart: '11-12-2019',
                dateEnd: '15-12-2019',
                isClose: false,
                description: 'En gros c"est de lire Vuejs seulement simple'
              }
            ]
          }
        ]
      }
    ],
    user: { id: '', email: '' },
    loading: null,
    error: null,
    nameProject: [],
    loadedProjects: [],
    uid: null,
    email: null,
    loadedTasks: []
  },
  mutations: {
    /* mutations, permet de faire le set du state */
    // setUser, permet de set le user
    setUser (state, payload) {
      state.user = payload
    },
    createProject (state, payload) {
      state.loadedProjects.push(payload)
    },
    clearError (state) {
      state.error = null
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    createTodolistPush (state, payload) {
      state.todolist.push(payload)
    },
    setNameProject (state, payload) {
      state.nameProject.push(payload)
    },
    setDataLoading (state, payload) {
      state.todolist = payload.todolist
    },
    setUidUser (state) {
      state.uid = firebase.auth().currentUser.uid
    },
    setEmail (state) {
      state.email = firebase.auth().currentUser.email
    },
    setLoadedProjects (state, payload) {
      state.loadedProjects = payload
    },
    setLoadedTasks (state, payload) {
      state.loadedTasks = payload
    },
    createTask (state, payload) {
      state.loadedTasks.push(payload)
    }
  },
  actions: {
    // Cette méthode permet de créer un user sur firebase
    // .then : on récupère le response
    signupUser ({ commit }, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            const newUser = {
              id: user.uid,
              email: user.email
            }
            commit('setUser', newUser)
            commit('setUidUser')
            commit('setEmail')
            /* var userAdded = newUser
            console.log(userAdded.id)
            console.log(userAdded.email) */
            /* console.log(firebase.auth().currentUser.uid)
            console.log(firebase.auth().currentUser.email) */
          }
        )
        .catch(
          error => {
            commit('setError', error)
            console.log(error)
          }
        )
    },
    signInUser ({ commit }, payload) {
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            const signedUser = {
              id: user.uid,
              email: user.email
            }
            commit('setUser', signedUser)
            commit('setUidUser')
            commit('setEmail')
            /* var userAdded = signedUser
            console.log(userAdded.uid)
            console.log(userAdded.email) */
          }
        )
        .catch(
          (error) => {
            commit('setError', error)
            console.log(error)
          }
        )
    },
    createTodoList ({ commit }, payload) {
      const todolist = {
        id: payload.id,
        email: payload.email,
        project: payload.project
      }
      firebase.database().ref('todolist').push(todolist)
        .then((data) => {
          const key = data.key
          /* console.log(key) */
          commit('createTodolistPush', { todolist, ids: key })
        })
        .catch((error) => {
          console.log(error)
        })
      commit('createTodolistPush', todolist)
    },
    createNameProject ({ commit }, payload) {
      const projectName = payload.name
      // console.log(projectName)
      commit('setNameProject', projectName)
    },
    /**
     * Projects
     */
    createProject ({ commit, getters }, payload) {
      let key
      const project = {
        title: payload.title,
        description: payload.description,
        createdAt: new Date(),
        createdBy: getters.user.id
      }
      firebase.database().ref('projects').push(project)
        .then((data) => {
          key = data.key
          return key
        })
        .then(() => {
          commit('createProject', {
            id: key
          })
        })
        .catch((error) => {
          console.log(error)
        })
    },
    loadProjects ({ commit }) {
      // commit('setLoading', true)
      firebase.database().ref('projects').once('value')
        .then((data) => {
          const projects = []
          const obj = data.val()
          for (let key in obj) {
            projects.push({
              id: key,
              title: obj[key].title,
              description: obj[key].description,
              createdAt: obj[key].createdAt,
              createdBy: obj[key].createdBy,
              todolist: obj[key].todolist
            })
          }
          commit('setLoadedProjects', projects)
          // commit('setNameProject', projects.title)
          // commit('setLoading', false)
        })
        .catch(
          (error) => {
            console.log(error)
            // commit('setLoading', false)
          })
    },

    /**
     * Tasks
     */
    createTask ({ commit, getters }, payload) {
      let key
      const task = {
        projectId: payload.projectId,
        title: payload.title,
        taskDescription: payload.description,
        createdAt: new Date(),
        taskStart: payload.taskStart,
        taskEnd: payload.taskEnd,
        taskPriority: payload.taskPriority,
        createdBy: getters.user.id
      }
      firebase.database().ref('tasks').push(task)
        .then((data) => {
          key = data.key
          return key
        })
        .then(() => {
          commit('createTask', {
            id: key
          })
        })
        .catch((error) => {
          console.log(error)
        })
    },
    loadTasks ({ commit }) {
      // commit('setLoading', true)
      firebase.database().ref('tasks').once('value')
        .then((data) => {
          const tasks = []
          const obj = data.val()
          for (let key in obj) {
            tasks.push({
              id: key,
              title: obj[key].title,
              taskDescription: obj[key].taskDescription,
              createdAt: obj[key].createdAt,
              createdBy: obj[key].createdBy,
              projectId: obj[key].projectId,
              taskStart: obj[key].taskStart,
              taskEnd: obj[key].taskEnd
            })
          }
          commit('setLoadedTasks', tasks)
          // commit('setNameProject', tasks.title)
          // commit('setLoading', false)
        })
        .catch(
          (error) => {
            console.log(error)
            // commit('setLoading', false)
          })
    },

    autoSignIn ({ commit }, payload) {
      commit('setUser', { id: payload.uid, registeredTodolist: [] })
    },
    logout ({ commit }) {
      firebase.auth().signOut()
      commit('setUser', null)
    }/* ,
    uidOfUser ({ commit }, payload) {
      firebase.auth().currentUser.uid
      commit('setUidUser', payload.uid)
    } */
  },
  getters: {
    user (state) {
      return state.user
    },
    loadedProjects (state) {
      return state.loadedProjects
    },
    getNameProject (state) {
      return state.nameProject
    },
    getAllTodolist (state) {
      return state.todolist
    },
    getUid (state) {
      return state.uid
    },
    getUserEmail (state) {
      return state.email
    },
    loadedTasks (state) {
      return state.loadedTasks
    }
  }
})
