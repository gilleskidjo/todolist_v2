import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bulma/css/bulma.min.css'
import 'bulma-carousel/dist/css/bulma-carousel.min.css'
import * as firebase from 'firebase'
import { store } from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBsBSPZoahl_TsSrHZrDBVYxICwzuiBjlA',
      authDomain: 'todolistvuejs-61838.firebaseapp.com',
      databaseURL: 'https://todolistvuejs-61838.firebaseio.com',
      projectId: 'todolistvuejs-61838',
      storageBucket: 'todolistvuejs-61838.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadProjects')
    this.$store.dispatch('loadTasks')
  }
}).$mount('#app')
